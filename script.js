"use strict"
/* 

Теоретичні питання
1. Які способи JavaScript можна використовувати для створення та додавання нових DOM-елементів?
// Є два способи створення DOM вузлів:

// document.createElement(tag)
// Створює новий елемент з заданим тегом:

// let div = document.createElement('div');
// document.createTextNode(text)
// Створює новий текстовий вузол з заданим текстом:

// let textNode = document.createTextNode('От і я');
// У більшості випадків нам потрібно створювати саме елементи, такі як div для повідомлень.

2. Опишіть покроково процес видалення одного елементу (умовно клас "navigation") зі сторінки.

1. Отримання посилання на елемент з класом "navigation" за допомогою методу document.querySelector('.navigation').
2. якщо елемент був знайдений він видаляється з його батьківського елементу за допомогою методу remove.



3. Які є методи для вставки DOM-елементів перед/після іншого DOM-елемента?
Для цього ми можемо використовувати інший, досить універсальний метод: elem.insertAdjacentHTML(where, html).

Перший параметр це кодове слово, яке вказує куди вставляти відносно elem. Його значення має бути одним з наступних:

"beforebegin" – вставити html безпосередньо перед elem,
"afterbegin" – вставити html в elem, на початку,
"beforeend" – вставити html в elem, в кінці,
"afterend" – вставити html безпосередньо після elem.
Другим параметром є рядок HTML, який вставляється “як HTML”.

Ось більше методів вставки, вони вказують куди саме буде вставлено вміст:

node.append(...вузли або рядки) – додає вузли або рядки в кінець node,
node.prepend(...вузли або рядки) – вставляє вузли або рядки на початку node,
node.before(...вузли або рядки) – вставляє вузли або рядки попереду node,
node.after(...вузли або рядки) – вставляє вузли або рядки після node,
node.replaceWith(...вузли або рядки) – замінює node заданими вузлами або рядками.
Аргументами цих методів є довільний список DOM вузлів або текстові рядки(які автоматично перетворюються на текстові вузли).





Практичні завдання
 1. Створіть новий елемент <a>, задайте йому текст "Learn More" і атрибут href з посиланням на "#". Додайте цей елемент в footer після параграфу.
 
 2. Створіть новий елемент <select>. Задайте йому ідентифікатор "rating", і додайте його в тег main перед секцією "Features".
 Створіть новий елемент <option> зі значенням "4" і текстом "4 Stars", і додайте його до списку вибору рейтингу.
 Створіть новий елемент <option> зі значенням "3" і текстом "3 Stars", і додайте його до списку вибору рейтингу.
 Створіть новий елемент <option> зі значенням "2" і текстом "2 Stars", і додайте його до списку вибору рейтингу.
 Створіть новий елемент <option> зі значенням "1" і текстом "1 Star", і додайте його до списку вибору рейтингу.

 */

// 1===============


const linkFooter = document.createElement("a");
linkFooter.innerText = "Learn More";
linkFooter.href = "#";
const footer = document.querySelector(".footer")
console.log(footer);
footer.append(linkFooter);

// 2============
const newSelect = document.createElement("select");
console.log(newSelect);
newSelect.id = 'rating';
const sectionFeatures = document.querySelector('.features');
console.log(sectionFeatures);
sectionFeatures.before(newSelect);


newSelect.options[0] = new Option("4 Stars");
newSelect.options[1] = new Option("3 Stars");
newSelect.options[2] = new Option("2 Stars");
newSelect.options[3] = new Option("1 Stars");

